import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/home/Index.vue';
import Thanks from './views/Thanks.vue';
import LegalTerms from './views/LegalTerms.vue';
import Collab from './views/Collab.vue';
import CatchThemAllGame from './views/games/catchThemAll/Game.vue';
import CharadeGame from './views/games/charade/Game.vue';
import CherryGame from './views/games/CherryOnTheCake/Game.vue';
import PizzasGame from './views/games/pizzas/Game.vue';
import PoulpeGame from './views/games/poulpe/Game.vue';
import Principles from './views/principles/Home.vue';
import HomogeneityPrinciple from './views/principles/Homogeneity.vue';
import ErrorHandlingPrinciple from './views/principles/ErrorHandling.vue';
import HickPrinciple from './views/principles/Hick.vue';
import MinimumActionsPrinciple from './views/principles/MinimumActions.vue';
import FittsPrinciple from './views/principles/Fitts.vue';
import Projet from './views/Projet/Index.vue';
import Quiz from './views/Quiz/Quiz.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior: function(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
        offset: { x: 0, y: 80 }
      }
    } else if (savedPosition) {
        return savedPosition;
    } else {
        return { x: 0, y: 0 }
    }  
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/thanks',
      name: 'thanks',
      component: Thanks
    },
    {
      path: '/legal-terms',
      name: 'legalTerms',
      component: LegalTerms
    },
       {
      path: '/collab',
      name: 'collab',
      component: Collab
    },
    {
      path: '/game/catch-them-all',
      name: 'catchThemAll',
      component: CatchThemAllGame
    },
    {
      path: '/game/charade',
      name: 'charade',
      component: CharadeGame
    },
    {
      path: '/game/CherryOnTheCake',
      name: 'CherryOnTheCake',
      component: CherryGame
    },
    {
      path: '/game/pizzas',
      name: 'pizzas',
      component: PizzasGame
    },
    {
      path: '/game/poulpe',
      name: 'poulpe',
      component: PoulpeGame
    },
    {
      path: '/principles',
      name: 'principles',
      component: Principles
    },
    {
      path: '/principles/homogeneity',
      name: 'homogeneityPrinciple',
      component: HomogeneityPrinciple
    },
    {
      path: '/principles/error-handling',
      name: 'errorHandlingPrinciple',
      component: ErrorHandlingPrinciple
    },
    {
      path: '/principles/hick',
      name: 'HickPrinciple',
      component: HickPrinciple
    },
    {
      path: '/principles/minimum-actions',
      name: 'minimumActionsPrinciple',
      component: MinimumActionsPrinciple
    },
    {
      path: '/principles/fitts',
      name: 'FittsPrinciple',
      component: FittsPrinciple
    },
    {
      path: '/Projet',
      name: 'Projet',
      component: Projet
    },
    {
      path: '/quiz/:id',
      name: 'Quiz',
      component: Quiz
    },
  ],
})
