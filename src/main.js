import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/scss/main.scss'

Vue.config.productionTip = false

Vue.use(BootstrapVue);

// if (process.env.NODE_ENV !== 'production') {
//   const VueAxe = require('vue-axe')
//   Vue.use(VueAxe, {
//     config: {
//       // ...
//       rules: [
//         { id: 'heading-order', enabled: true },
//         { id: 'label-title-only', enabled: true },
//         // and more
//       ]
//     }
//   })
//   Vue.config.productionTip = false
// }

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
